from ticket.models import *

seat_table = [
    ['X', range(1, 40, 2), range(40, 0, -2)],
    ['Z', range(1, 42, 2), range(42, 0, -2)],
    ['Y', range(1, 44, 2), range(44, 0, -2)],
    ['V', range(1, 44, 2), range(44, 0, -2)],
    ['U', range(1, 44, 2), range(44, 0, -2)],
    ['T', range(1, 42, 2), range(42, 0, -2)],
    ['S', range(1, 42, 2), range(42, 0, -2)],
    ['R', range(1, 42, 2), range(42, 0, -2)],
    ['P', range(1, 40, 2), range(40, 0, -2)],
    ['O', range(1, 40, 2), range(40, 0, -2)],
    ['N', range(1, 40, 2), range(40, 0, -2)],
    ['M', range(1, 38, 2), range(38, 0, -2)],
    ['L', range(1, 38, 2), range(38, 0, -2)],
    ['K', range(1, 38, 2), range(38, 0, -2)],
    ['J', range(1, 36, 2), range(36, 0, -2)],
    ['I', range(1, 34, 2), range(36, 0, -2)],
    ['H', range(1, 34, 2), range(36, 0, -2)],
    ['G', range(1, 32, 2), range(30, 0, -2)],
    ['F', range(1, 30, 2), range(30, 0, -2)],
    ['E', range(1, 30, 2), range(30, 0, -2)],
    ['D', range(1, 28, 2), range(28, 0, -2)],
    ['C', range(1, 28, 2), range(28, 0, -2)],
    ['B', range(1, 26, 2), range(26, 0, -2)],
    ['A', range(1, 26, 2), range(24, 0, -2)],
]

for row in seat_table:
    key = row[0]
    nums = list(row[1]) + list(row[2])
    for num in nums:
        q = Place.objects.all().filter(code=key+str(num))
        if q.count() == 1:
            q[0].status = "AV"
            q[0].save()
        else:
            Place.objects.create(
                code=key+str(num),
                status="AV"
            )
