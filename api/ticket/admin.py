from django.contrib import admin

from ticket.models import *

admin.site.register(Place)
admin.site.register(TicketOwner)
admin.site.register(Ticket)
