from django.db import models

__all__ = ['Place', 'TicketOwner', 'Ticket']

class TicketOwner(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)

class Ticket(models.Model):
    owner = models.ForeignKey(
        TicketOwner,
        on_delete=models.CASCADE,
        related_name='tickets'
    )

class Place(models.Model):
    code = models.CharField(max_length=100)
    status = models.CharField(
        max_length=100,
        choices=[
            ('AV', 'Available'),
            ('RES', 'Reserved')
        ]
    )
    ticket = models.ForeignKey(
        Ticket,
        on_delete=models.CASCADE,
        related_name='places',
        null=True,
        blank=True
    )
