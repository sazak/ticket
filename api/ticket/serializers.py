from rest_framework import serializers

from ticket.models import *

class TicketSerializer(serializers.ModelSerializer):
    place = serializers.SerializerMethodField()
    owner = serializers.SerializerMethodField()

    class Meta:
        model = Ticket
        fields = ('id', 'place', 'owner')

    def get_place(self, obj):
        return obj.place.code

    def get_owner(self, obj):
        return {
            'first_name': obj.owner.first_name,
            'last_name': obj.owner.last_name,
            'email': obj.owner.email
        }

class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = '__all__'
