from django.urls import path, include

from ticket.views import *

urlpatterns = [
    path('reserve/', ReserveView.as_view()),
    path('tickets/', TicketsView.as_view()),
    path('places/', PlacesView.as_view()),
    path('places/reserved/', ReservedPlacesView.as_view()),
]
