from django.core.exceptions import ObjectDoesNotExist

from rest_framework.views import APIView
from rest_framework import generics, status
from rest_framework.response import Response

from ticket.models import *
from ticket.serializers import *

__all__ = ['ReserveView', 'TicketsView', 'PlacesView', 'ReservedPlacesView']

class TicketsView(generics.ListAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer

class PlacesView(generics.ListAPIView):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer

class ReservedPlacesView(APIView):
    def get(self, request):
        ret = []
        for obj in Place.objects.all().filter(status="RES"):
            ret.append(obj.code)

        return Response(ret, status=200)

class ReserveView(APIView):
    def post(self, request):
        for place_code in request.data["seats"]:
            try:
                place = Place.objects.get(code=place_code)
            except ObjectDoesNotExist:
                return Response({
                    'code': 'PLACE_DNE'
                }, status=400)

            if place.status != 'AV':
                return Response({
                    'code': 'PLACE_RES'
                }, status=401)

        owner = TicketOwner.objects.create(
            first_name = request.data['owner_first_name'],
            last_name = request.data['owner_last_name'],
            email = request.data['owner_email']
        )

        ticket = Ticket.objects.create(
            owner = owner
        )

        for place_code in request.data["seats"]:
            place = Place.objects.get(code=place_code)

            ticket.places.add(place)

            place.status = "RES"
            place.save()

        ticket.save()

        return Response({}, status=201)
